function distance(first, second){
	
	if(!Array.isArray(first) || !Array.isArray(second)){
		throw new Error('InvalidType');
	}
	
	if(first.length === 0 && second.length === 0){
		return 0;
	}
	
	let set1 = new Set();
	let set2 = new Set();
	
	for(let i=0;i<first.length;i++){
		set1.add(first[i]);
	}
	for(let i=0;i<second.length;i++){
		set2.add(second[i]);
	}
	
	array1 = Array.from(set1);
	array2 = Array.from(set2);
	
	let aux = 0;
	for(let i=0;i<array1.length;i++){
		for(let j=0;j<array2.length;j++){
			if(array1[i]===array2[j]){
				aux+=2;
				break;
			}
		}	
	}
	
	let sum = array1.length + array2.length;
	return sum - aux;
	
}


module.exports.distance = distance